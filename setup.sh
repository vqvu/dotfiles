#!/bin/bash

CONFIG_PATH="$(cd "$(dirname "$0")" && pwd)"
SHELL_FUNCTIONS_ROOT="${CONFIG_PATH}/shell"
PREAMBLE="${CONFIG_PATH}/preamble.sh"

source "${PREAMBLE}"

init_script

print_help() {
    echo "Usage: $0 [options]"
    echo ""
    echo "Options:"
    echo "  -e <environment>   Environment."
}

_on_exit_cleanup() {
    if [[ -n "${_DELETE_SCRATCH}" ]]; then
        simple_action "Removing scratch dir" rm -rf "${SCRATCH_PATH}"
    fi
}

check_web() {
    local protocol retcode
    protocol="$1"

    if curl --connect-timeout 5 -s "${protocol}://google.com/" > /dev/null; then
        success_msg "${protocol} works."
    else
        error_msg "${protocol} doesn't work. Fix your network connection."
        return "${retcode}"
    fi
}

_DELETE_SCRATCH=
trap _on_exit_cleanup EXIT

source "${CONFIG_PATH}/bootstrap.frag.sh"

header_msg "Verifying network connectivity."
check_web http
check_web https

if is_debian; then
    action_start "Updating apt."
    action_exec sudo apt-get update
    action_end
fi

header_msg "Installing setup dependencies."
if is_new_git_rev "${DOTFILES_PATH}" dotfiles; then
    action_start "Fetching submodules"
    action_exec git submodule --quiet update --init --recursive
    action_exec save_git_rev "${DOTFILES_PATH}" dotfiles
    action_end
else
    success_msg "Submodules up to date."
fi

if is_mac; then
    if ! which brew >& /dev/null; then
        action_start "Installing brew"
        action_exec /usr/bin/ruby "${DOTFILES_EXTERNAL_PATH}/Homebrew-install/install" < /dev/null
        action_end
    else
        success_msg "brew already installed."
    fi

    # Install the gnu tools
    brew_install coreutils

    # Set the PATH to point to point to the GNU tools
    export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"

    # Now we have GNU tools.
fi

exec_all_modules pre-install-setup

header_msg "Installing software"
if is_mac; then
    for pkg in meld git zsh node go rust vim the_silver_searcher diff-so-fancy; do
        brew_install "${pkg}"
    done

    for pkg in meld firefoxdeveloperedition intellij-idea iterm2; do
        brew_cask_install "${pkg}"
    done
elif is_centos; then
    for pkg in the_silver_searcher; do
        yum_install "${pkg}"
    done
elif is_debian; then
    for pkg in silversearcher-ag zsh vim-nox git-credential-oauth; do
        apt_install "${pkg}"
    done
fi

cd "${DOTFILES_EXTERNAL_PATH}/powerline-fonts"
if is_new_git_rev . powerline-fonts; then
    action_start "Installing powerline fonts"
    action_exec bash install.sh
    action_exec save_git_rev . powerline-fonts
    action_end
else
    success_msg "powerline fonts already installed."
fi

if is_linux; then
    _diff-so-fancy_is_installed() {
        local version="$2"

        which diff-so-fancy > /dev/null
        {
            # Needs to run in a subshell so we can clear the exit code.
            # diff-so-fancy returns an error even when called with --version.
            diff-so-fancy --version
            exit 0
        } |& grep --quiet "${version}"
    }
    _diff-so-fancy_install() {
        local version="$2"

        download "https://github.com/so-fancy/diff-so-fancy/releases/download/v${version}/diff-so-fancy" diff-so-fancy
        cp -f -r "${CACHE_PATH}/diff-so-fancy" "${LOCAL_INSTALL_PATH}/bin"
        chmod +x "${LOCAL_INSTALL_PATH}/bin/diff-so-fancy"
    }
    install_pkg _diff-so-fancy diff-so-fancy 1.4.4
fi

header_msg "Configuring zsh."
if [[ "$(getent passwd victor | cut -d: -f7)" != "$(which zsh)" ]]; then
    simple_action "Updating login shell to $(which zsh)." sudo chsh -s "$(which zsh)" "${USER}"
else
    success_msg "Default shell is already zsh."
fi
for f in .zshenv .zsh; do
    link_home "$f" "${DOTFILES_PATH}/$f"
done
link_external "dircolors-solarized/dircolors.256dark" "${HOME}/.dircolors"

header_msg "Configuring git."
FILES=".gitk .gitconfig"
for f in ${FILES}; do
    link_home "$f" "${DOTFILES_PATH}/$f"
done
link "${DOTFILES_PATH}/git-ssh-sign" "${LOCAL_INSTALL_PATH}/bin/git-ssh-sign"

header_msg "Configuring GPG."
mkdir -p ~/.gnupg
link "${DOTFILES_PATH}/gpg.conf" ~/.gnupg/gpg.conf

exec_module "${DOTFILES_PATH}/vim/setup.sh"

header_msg "Cleaning up."
is_mac && simple_action "Cleaning up brew" brew cleanup
trap - EXIT
_on_exit_cleanup

header_msg "Manual steps."
print_msg "You need to do the following manual steps (if not already):"
is_mac && task_msg "Import the '${DOTFILES_PATH}/Solarized Dark.terminal' profile into your Terminal.app."
task_msg "Reboot your shell."

deinit_script
