#!/bin/bash
source "${SHELL_FUNCTIONS_ROOT}/env.sh"

init_script() {
    set -e
    set -o pipefail
    set -o nounset

    _PRINT_FIRST_TIME_HEADER=true
    _PRINT_PREFIX=
    exec 3>&1

    increment_print_prefix() { _PRINT_PREFIX="${_PRINT_PREFIX}  "; }
    decrement_print_prefix() { _PRINT_PREFIX="${_PRINT_PREFIX%  }"; }

    print_msg() {
        _PRINT_FIRST_TIME_HEADER=false
        echo -e "${_PRINT_PREFIX}$@" >&3
    }

    _ACTION_RETVAL=()
}

deinit_script() {
    set +e
    set +o pipefail
    set +o nounset

    unset -v _PRINT_FIRST_TIME_HEADER
    unset -v _PRINT_PREFIX
    exec 3>&-

    unset -f increment_print_prefix
    unset -f decrement_print_prefix

    print_msg() {
        echo -e "${_PRINT_PREFIX}$@"
    }

    unset -v _ACTION_RETVAL
}


# === Printing ===
RED=$'\e[0;31m'
GREEN=$'\e[0;32m'
YELLOW=$'\e[0;33m'
BLUE=$'\e[0;34m'
MAGENTA=$'\e[0;35m'
CYAN=$'\e[0;36m'
DEFAULT=$'\e[0m'

print_msg() {
    echo -e "${_PRINT_PREFIX}$@"
}

header_msg() {
    if [[ "${_PRINT_FIRST_TIME_HEADER}" = true ]]; then
        _PRINT_FIRST_TIME_HEADER=false
    else
        print_msg
    fi
    print_msg "${BLUE}==> $@${DEFAULT}"
}

info_msg() { print_msg "  $@"; }
success_msg() { print_msg "${GREEN}✓ $@${DEFAULT}"; }
error_msg() { print_msg "${RED}✗ $@${DEFAULT}"; return 1; }
task_msg() { print_msg "• $@"; }

# === Long-running actions. ===

VERBOSE="${VERBOSE:-none}"

# Validate verbosity
case "${VERBOSE}" in
    none);;
    error);;
    all);;
    *)
        error_msg "'${VERBOSE}' is not a valid verbosity. Must be one of: {none, error, all}."
        ;;
esac

action_start() {
    info_msg "$@..."
    _ACTION_RETVAL+=(0)
    increment_print_prefix
}

action_save_retval() {
    if is_zsh; then
        _ACTION_RETVAL[${#_ACTION_RETVAL[@]}]="$?"
    else
        _ACTION_RETVAL[${#_ACTION_RETVAL[@]}-1]="$?"
    fi
    return 0
}

_action_retval() {
    if is_zsh; then
        echo "${_ACTION_RETVAL[${#_ACTION_RETVAL[@]}]}"
    else
        echo "${_ACTION_RETVAL[${#_ACTION_RETVAL[@]}-1]}"
    fi
}

_action_exec() {
    local verbosity

    verbosity="$1" && shift

    if [[ "$(_action_retval)" -eq 0 ]]; then
        case "${verbosity}" in
        none)
            "$@" &> /dev/null || action_save_retval
            ;;
        error)
            "$@" > /dev/null || action_save_retval
            ;;
        all)
            "$@" || action_save_retval
            ;;
        *)
            error_msg "Script bug! Invalid verbosity: ${verbosity}."
            ;;
        esac
    fi
}

action_exec_verbose_full() { _action_exec all "$@"; }
action_exec() { _action_exec "${VERBOSE}" "$@"; }

action_end() {
    local retval
    retval="$(_action_retval)"

    decrement_print_prefix
    [[ "${retval}" -eq 0 ]] || print_msg "  ${RED}failed${DEFAULT}"

    if is_zsh; then
        _ACTION_RETVAL[${#_ACTION_RETVAL[@]}]=()
    else
        unset _ACTION_RETVAL[${#_ACTION_RETVAL[@]}-1]
    fi

    return "${retval}"
}

action_end_can_fail() {
    local retval
    retval="$(_action_retval)"

    decrement_print_prefix
    [[ "${retval}" -eq 0 ]] || print_msg "  failed (still ok)"

    if is_zsh; then
        _ACTION_RETVAL[${#_ACTION_RETVAL[@]}]=()
    else
        unset _ACTION_RETVAL[${#_ACTION_RETVAL[@]}-1]
    fi

    return 0
}

simple_action() {
    local action_name cmd
    action_name="$1" && shift
    cmd="$1" && shift

    action_start "${action_name}"
    action_exec "${cmd}" "$@"
    action_end
}

_install_pkg() {
    local pkg is_dep installer

    is_dep="$1" && shift
    installer="$1" && shift
    pkg="$1" && shift

    if ! ${installer}_is_installed "${pkg}" "$@"; then
        if [[ "${is_dep}" = true ]]; then
            action_start "Installing dependency ${pkg}"
        else
            action_start "Installing ${pkg}"
        fi

        action_exec ${installer}_install "${pkg}" "$@"
        action_end
    elif [[ "${is_dep}" != true ]]; then
        success_msg "${pkg} already installed."
    else
        return 0
    fi
}

install_pkg() { _install_pkg false "$@"; }
install_dep_pkg() { _install_pkg true "$@"; }

if is_mac; then
    _brew_install() { brew install "$1"; }
    _brew_is_installed() { test -n "$(brew ls --versions "$1")"; }
    _brew_cask_install() { brew cask install "$1"; }
    _brew_cask_is_installed() { brew cask ls --versions | grep -q "^$1 "; }

    brew_install() { install_pkg _brew "$@"; }
    brew_install_dep() { install_dep_pkg _brew "$@"; }
    brew_cask_install() { install_pkg _brew_cask "$@"; }
fi

if is_centos; then
    _yum_install() { sudo yum install -q -y "$1"; }
    _yum_is_installed() { yum list -q installed "$1" &> /dev/null; }

    yum_install() { install_pkg _yum "$@"; }
fi

if is_debian; then
    _apt_install() { sudo apt-get install -qq "$1"; }
    _apt_is_installed() { dpkg-query -W --showformat='${status}' "$1" |& grep -q "install ok installed"; }
    apt_install() { install_pkg _apt "$@"; }
    apt_install_dep() { install_dep_pkg _apt "$@"; }
fi

install_program() {
    local program installer
    program="$1" && shift
    installer="$1" && shift

    if which "${program}" > /dev/null; then
        success_msg "${program} already installed."
    else
        if [[ $# -eq 0 ]]; then
            action_start "Installing ${program}"
        else
            action_start "Installing ${program} (with $@)"
        fi
        action_exec ${installer} "$@"
        action_end
    fi
}

# === Util ===

is_link_installed() {
    local target name
    target="$1"
    name="$2"

    test "z$(readlink -m "${target}")" = "z$(readlink -m "${name}")"
}

link() {
    local target name
    target="$1"
    name="$2"

    if is_link_installed "${target}" "${name}"; then
        success_msg "symlink ${name} is correct."
    else
        info_msg "Mapping ${name} -> ${target}."
        if [[ -d "${name}" ]]; then
            rm -rf "${name}"
        fi
        ln -sfT "${target}" "${name}"
    fi
}

file_age_epoch_seconds() { stat -c '%Y' "$1"; }

checksum() {
    local chksum_cmd
    which sha1sum &> /dev/null && chksum_cmd=sha1sum || chksum_cmd=shasum
    if [[ -f "$1" ]]; then
        ${chksum_cmd} "$1" | awk '{ print $1 }'
    else
        echo -n | ${chksum_cmd} | awk '{ print $1 }'
    fi
}

variable_repr() {
    local var export_str str
    var="$1"

    if [[ $# -ge 2 && "z$2" = "ztrue" ]]; then
        export_str="export "
    else
        export_str=
    fi

    # Read returns 1 at EOF. We need to supress that in case this is being used
    # in a script with set -e.
    read -r -d '' str <<-END && true
    if [[ -n "\${${var}+x}" ]]; then
        echo "${export_str}${var}='\${${var}}'"
    else
        echo "unset -v ${var}"
    fi
END

    eval "${str}" && true
    return 0
}
