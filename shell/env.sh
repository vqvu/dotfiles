#!/bin/sh

# This file is used by fasdrc, so it should be compatible with sh.

# Environment state detection
is_mac() { test "`uname`" = 'Darwin'; }
is_linux() { test "`uname`" = 'Linux'; }
is_centos() { is_linux && lsb_release -d | grep -qE "CentOS|Red Hat Enterprise Linux"; }
is_centos6() { is_centos && lsb_release -r | grep -qE "6\\.[0-9]+"; }
is_debian() { is_linux && lsb_release -i | grep -qE "Debian|Ubuntu"; }
is_wsl() { test -n "${WSL_DISTRO_NAME}"; }
first_ip() {
    ifconfig | grep 'inet ' | awk '{ print $2 }'  | grep -v '127\.0\.0\.1' | head -n 1
}
has_ip() {
    test -n "`first_ip`"
}
is_zsh() { test -n "${ZSH_NAME:-}"; }

