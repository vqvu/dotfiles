#!/bin/bash
if [[ "$(uname)" = "Linux" ]] && ! which lsb_release > /dev/null; then
    echo -e "\e[0;31m✗ lsb_release is not installed. Please install it.\e[0m"
    exit 1
fi

source "${SHELL_FUNCTIONS_ROOT}/functions.sh"

# Setup paths
DOTFILES_PATH="${HOME}/.dotfiles"
DOTFILES_EXTERNAL_PATH="${DOTFILES_PATH}/external"
LOCAL_INSTALL_PATH="${HOME}/.local"

# Installer paths
ENV_STATE_PATH="${HOME}/.env-state"
INSTALL_STATE_PATH="${ENV_STATE_PATH}/install"
INSTALL_META_PATH="${INSTALL_STATE_PATH}/metadata"
CACHE_PATH="${INSTALL_STATE_PATH}/cache"
SCRATCH_PATH="${INSTALL_STATE_PATH}/scratch"

# === Modules. ===

exec_module() {
    (
    SCRIPT_PATH="$(dirname "$1")"
    source "$1"
    )
}

exec_all_modules() {
    local f
    for f in $(find "${DOTFILES_PATH}" -mindepth 2 -maxdepth 2 -name "$1.sh"); do
        exec_module "$f"
    done
}

link_home() { link "$2" "${HOME}/$1"; }
link_external() { link "${DOTFILES_EXTERNAL_PATH}/$1" "$2"; }

is_new_git_rev() {
    local dir name rev_file res
    dir="$1"
    name="$2"
    rev_file="${INSTALL_META_PATH}/${name}.last-visited-rev"

    cd "${dir}"

    if ! git status &> /dev/null; then
        error_msg "'$(pwd)' is not a git repository." || return $?
    fi

    if [[ ! -e "${rev_file}" ]]; then
        cd "${OLDPWD}"
        return 0
    fi

    if [[ ! -f "${rev_file}" ]]; then
        error_msg "The revision file (${rev_file}) is not a file!" || return $?
    fi

    test "z$(cat "${rev_file}")" != "z$(git rev-parse HEAD)"
    res="$?"
    cd "${OLDPWD}"

    return "${res}"
}

save_git_rev() {
    local dir name rev_file res
    dir="$1"
    name="$2"
    rev_file="${INSTALL_META_PATH}/${name}.last-visited-rev"

    cd "${dir}"
    git rev-parse HEAD > "${rev_file}"
    res="$?"
    cd "${OLDPWD}"

    return "${res}"
}

_external_install() {
    local external_repo
    external_repo="$1"
    _external_"${external_repo}"_install "$@" || return $?
    save_git_rev "${DOTFILES_EXTERNAL_PATH}/${external_repo}" "${external_repo}" || return $?
}

_external_is_installed() {
    ! is_new_git_rev "${DOTFILES_EXTERNAL_PATH}/$1" "$1"
}

install_external() { install_pkg _external "$@"; }

is_file_modified() {
    local file name chksum_file chksum res
    file="$1"
    name="$2"
    chksum_file="${INSTALL_META_PATH}/${name}.chksum"

    chksum="$(checksum "${file}")"

    [[ ! -e "${chksum_file}" ]] && return 0

    if [[ ! -f "${chksum_file}" ]]; then
        error_msg "The checksum file (${chksum_file}) is not a file!" || return $?
    fi

    test "z${chksum}" != "z$(cat "${chksum_file}")"
}

save_file_checksum() {
    local file name chksum_file
    file="$1"
    name="$2"
    chksum_file="${INSTALL_META_PATH}/${name}.chksum"
    checksum "${file}" > "${chksum_file}"
}

download() {
    local url file chksum
    url="$1"
    file="$2"
    [[ $# -ge 3 ]] && chksum="$3" || chksum=

    if [[ ! -f "${CACHE_PATH}/${file}" ]]; then
        action_start "Downloading ${url} to ${file}"
        action_exec rm -rf "${CACHE_PATH}/${file}"
        action_exec curl -L --silent --fail "${url}" -o "${SCRATCH_PATH}/${file}.curl-tmp-dl"
        action_exec mv "${SCRATCH_PATH}/${file}.curl-tmp-dl" "${CACHE_PATH}/${file}"
        action_end || return $?
    fi

    if [[ -n "${chksum}" ]] && [[ "z$(checksum "${CACHE_PATH}/${file}")" != "z${chksum}" ]]; then
        error_msg "Corrupted ${file}. Checksum does not match. URL: ${url}" || return $?
    fi
}

download_tar() {
    local url file dir suffix
    url="$1"
    file="$2"
    dir=${file%.tar*}
    suffix="${file#${dir}}"

    if [[ ! -d "${dir}" ]]; then
        download "${url}" "${file}" || return $?
        cd "${CACHE_PATH}" || return $?
        tar -xf "${file}" || return $?
    fi
}
