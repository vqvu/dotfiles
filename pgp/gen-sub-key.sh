#!/bin/bash
set -eu

if [[ $# -eq 0 ]]; then
    read -e -p "Subkey name: " SUBKEY_NAME
    read -e -p "Master key ID: " MASTER_KEYID
    read -e -p "Public key file: " PUB_KEY_FILE
    read -e -p "Private key file: " PRIV_KEY_FILE
elif [[ $# -eq 4 ]]; then
    SUBKEY_NAME="$1"
    MASTER_KEYID="$2"
    PUB_KEY_FILE="$3"
    PRIV_KEY_FILE="$4"
else
    echo "Usage: $0 <subkeyname> <masterkeyid> <pubkeyfile> <seckeyfile>"
    exit 1
fi

cleanup() {
    ERRCODE=$?
    rm -rf "${GPG_HOME}"
    exit "${ERRCODE}"
}

GPG_HOME=.tmp-gnupg
GPG="gpg --homedir ${GPG_HOME}"

mkdir -p "${GPG_HOME}"
trap cleanup ERR

chmod 700 "${GPG_HOME}"

exportsubkey() {
    echo "Exporting key: $1 to $2.sec.asc."
    ${GPG} --armor --export-secret-subkeys "$1" > "${2}.sec.asc"
}

${GPG} --import "${PUB_KEY_FILE}"
${GPG} --allow-secret-key-import --import "${PRIV_KEY_FILE}"

ULTIMATE_TRUST=5
echo -ne "${ULTIMATE_TRUST}\ny\n" | ${GPG} --command-fd 0 --edit-key "${MASTER_KEYID}" trust || test $? -eq 2

# RSA
SIG_KEY_ALGO=4
echo -ne "${SIG_KEY_ALGO}\n4096\n1y\nsave\n" | ${GPG} --command-fd 0 --edit-key "${MASTER_KEYID}" addkey || test $? -eq 2
read -e -p "Sign subkey id? " SIG_KEYID
exportsubkey "${SIG_KEYID}" "${SUBKEY_NAME}.sign"

