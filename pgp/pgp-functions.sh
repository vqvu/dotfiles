#!/bin/bash

GPG_EDITKEY_DSA_SIGN_ONLY=3
GPG_EDITKEY_RSA_SIGN_ONLY=4
GPG_EDITKEY_DSA_ENCRYPT_ONLY=5
GPG_EDITKEY_RSA_ENCRYPT_ONLY=6

gpg_cleanup() {
    local ERRCODE=$?
    rm -rf "${GPG_HOME}"
    exit "${ERRCODE}"
}

gpg_init() {
    mkdir -p "${GPG_HOME}"
    trap cleanup_gpg ERR
    chmod 700 "${GPG_HOME}"
}

gpg_extract_key_id() {
    grep --only-matching "^gpg: key [0-9A-Z]+:" | cut -d' ' -f 3 | grep --only-matching "[0-9A-Z]+"
}

gpg_run() {
    "${GPG}" --homedir "${GPG_HOME}" "$@"
}

gpg_edit_key() {
    local key_id edit_command answers
    key_id="$1"
    edit_command="$2"
    answers="$3"
    echo -ne "${answers}" | \
        gpg_run --command-fd 0 --edit-key "${key_id}" "${edit_command}" || \
        test "$?" -eq 2
}

gpg_import_master_key() {
    local pub_key_file priv_key_file pub_key_id priv_key_id
    pub_key_file="$1.pub.asc"
    priv_key_file="$1.priv.asc"

    pub_key_id="$(gpg_run --import "${pub_key_file}" | gpg_extract_key_id)"
    priv_key_id="$(gpg_run --allow-secret-key-import --import "${priv_key_file}" | gpg_extract_key_id)"

    test "z${pub_key_id}" -ne "z${priv_key_id}"

    local ULTIMATE_TRUST=5
    gpg_edit_key "${pub_key_id}" trust "${ULTIMATE_TRUST}\ny\n"

    echo "${pub_key_id}"
}

gpg_gen_subkey() {
    local key_type master_key_id
    key_type="$1"
    master_key_id="$2"
    gpg_edit_key "${master_key_id}" addkey "${key_type}\n4096\n0\ny\ny\n"
}
