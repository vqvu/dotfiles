#!/bin/bash

set -e

if [ $# -ne 4 ]; then
    echo "Usage: $0 <masterkeyid> <pubkeyfile> <enc_subkeyfile> <sign_subkeyfile>"
    exit 1
fi

GPG=gpg

MASTER_KEYID=$1
PUB_KEY_FILE=$2
ENC_SUBKEY_FILE=$3
SIGN_SUBKEY_FILE=$3

${GPG} --import ${PUB_KEY_FILE}
${GPG} --allow-secret-key-import --import ${ENC_SUBKEY_FILE} ${SIGN_SUBKEY_FILE}

ULTIMATE_TRUST=5
echo -ne "${ULTIMATE_TRUST}\ny\n" | ${GPG} --command-fd 0 --edit-key ${MASTER_KEYID} trust || test $? -eq 2

