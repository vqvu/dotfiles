#!/bin/bash
set -eu
set -o pipefail

if [[ $# -eq 0 ]]; then
    read -e -p "Name: " NAME
    read -e -p "Email: " EMAIL
    read -e -p "Comment (can be blank): " COMMENT
elif [[ $# -ge 2 ]]; then
    NAME="$1"
    EMAIL="$2"
    COMMENT="$3"
else
    echo "Usage: $0 <name> <email> [comment]"
    exit 1
fi

cleanup() {
    ERRCODE=$?
    rm -rf "${GPG_HOME}"
    exit "${ERRCODE}"
}


GPG_HOME=.tmp-gnupg

NAME="$1"
EMAIL="$2"
COMMENT="$3"

GPG="gpg2 --homedir ${GPG_HOME}"

DIGEST_PREF="SHA512 SHA384 SHA256 SHA224"
CYPHER_PREF="AES256 AES192 AES CAST5 3DES"
COMPRESSION_PREF="ZLIB BZIP2 ZIP Uncompressed"

mkdir -p "${GPG_HOME}"
trap cleanup ERR

chmod 700 "${GPG_HOME}"

${GPG} --batch --gen-key <<EOF || test $? -eq 2
    %echo Generating a basic OpenPGP key for ${NAME} (${COMMENT}) ${EMAIL}.
    Key-Type: RSA
    Key-Length: 4096
    Key-Usage: sign
    Subkey-Type: RSA
    Subkey-Length: 4096
    Subkey-Usage: encrypt
    Name-Real: ${NAME}
    $(test "${COMMENT}" && echo Name-Comment: "${COMMENT}")
    Name-Email: ${EMAIL}
    Expire-Date: 0
    Preferences: ${DIGEST_PREF} ${CYPHER_PREF} ${COMPRESSION_PREF}
    %ask-passphrase
    %echo About to commit. It will take some time to generate entropy.
    %commit
    %echo done
EOF

KEYID="${EMAIL}"

PUB_KEY_FILE="${KEYID}.pub.asc"
PRIV_KEY_FILE="${KEYID}.sec.asc"
PRIV_ENC_SUBKEY_FILE="${KEYID}.enc.sec.asc"
REVOKE_KEY_FILE="${KEYID}.revoke.asc"

${GPG} --list-secret-keys
read -e -p "Encrypt subkey id? " ENC_KEYID
read -e -p "Master key id? " MASTER_KEYID

${GPG} --armor --export "${KEYID}" > "${PUB_KEY_FILE}"
${GPG} --armor --export-secret-keys "${MASTER_KEYID}" > "${PRIV_KEY_FILE}"
${GPG} --armor --export-secret-subkeys "${ENC_KEYID}" > "${PRIV_ENC_SUBKEY_FILE}"
${GPG} --armor --gen-revoke "${KEYID}" > "${REVOKE_KEY_FILE}"

echo "Keys exported as ${PUB_KEY_FILE} and ${PRIV_KEY_FILE}."
echo "Revokation certificate in ${REVOKE_KEY_FILE}."

