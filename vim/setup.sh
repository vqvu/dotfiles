#/bin/bash
YCM_PATH="${HOME}/.vim/bundle/YouCompleteMe"
YCM_CORE_PATH="${YCM_PATH}/third_party/ycmd"

header_msg "Configurating vim."

mkdir -p ~/.vim
mkdir -p ~/.vim/bundle

for f in .vimrc
do
    link "${SCRIPT_PATH}/$f" "${HOME}/$f"
done

for f in after colors filetype.vim syntax vundle.vim
do
    link "${SCRIPT_PATH}/$f" "${HOME}/.vim/$f"
done

link_external "Vundle.vim" "${HOME}/.vim/bundle/vundle"

if is_file_modified "${SCRIPT_PATH}/vundle.vim" vim-plugins; then
    action_start "Installing vim plugins"
    action_exec vim -u "${SCRIPT_PATH}/vundle.vim" +PluginInstall +qall
    action_exec save_file_checksum "${SCRIPT_PATH}/vundle.vim" vim-plugins
    action_end
else
    success_msg "Vim plugins already installed."
fi

touch "${YCM_PATH}/.build-version"
cd "${YCM_PATH}"

if is_mac || is_linux; then
    YCM_CORE_FILE="${YCM_CORE_PATH}/ycm_core*.so"
fi

if [[ -n "$(ls "${YCM_CORE_PATH}/ycm_core"*.so 2> /dev/null)" ]] && ! is_new_git_rev "${YCM_PATH}" ycmd; then
    success_msg "YCMD already built."
else
    if is_mac; then
        brew_install_dep cmake
    elif is_debian; then
        for pkg in build-essential cmake python3-dev mono-complete golang nodejs openjdk-17-jdk openjdk-17-jre npm; do
            apt_install_dep "${pkg}"
        done

        if is_wsl; then
            simple_action "Fixing WSL interop. See https://github.com/microsoft/WSL/issues/5466." \
                sudo update-binfmts --disable cli
        fi
    fi

    local ycm_flags=()
    ycm_flags+=(--all)

    action_start "Compiling YCMD with ${ycm_flags[@]}. This will take a while"
    action_exec "${YCM_PATH}/install.py" "${ycm_flags[@]}"
    action_exec save_git_rev "${YCM_PATH}" ycmd
    action_end
fi

