syn region cudaTConfig          start="<<<" end=">>>"   keepend contains=cudaTConfigOperator
syn match  cudaTConfigOperator  ",\|<<<\|>>>"           contained

syn keyword cudaQualifier        __noinline__ __forceinline__

hi def link cudaTConfigOperator Operator
hi def link cudaQualifier       cudaStorageClass
"hi def link cudaTConfig         Label

