" The following matches one or two characters, but no more (e.g. not +++)
syn match rubyOperator             "+\@<!+\{1,2}+\@!"
syn match rubyOperator             "-\@<!-\{1,2}-\@!"
syn match rubyOperator             "&\@<!&\{1,2}&\@!"
syn match rubyOperator             "&\@<!|\{1,2}&\@!"
syn match rubyOperator             "<\@<!<\{1,2}[-<"0-9a-zA-Z]\@!"
syn match rubyOperator             ">\@<!>\{1,2}>\@!"

" The following matches exactly one (e.g. not //)
syn match rubyOperator             "[*/]\@<!/[*/]\@!"
syn match rubyOperator             "[*/]\@<!\*[*/]\@!"
syn match rubyOperator             "!\@<!!!\@!"

" The following matches exactly two (e.g. not = or ===)
syn match rubyOperator             "=\@<!==[=]\@!"
syn match rubyOperator             "=\@<!<=[<]\@!"
syn match rubyOperator             "=\@<!>=[>]\@!"
syn match rubyOperator             "=\@<!!=[!]\@!"
syn match rubyOperator             ">\@<!=>[=]\@!"

syn keyword rubyOperator    or and not
syn keyword rubyInclude     require_relative


"syn match  rubyOperator	 "\%([~!^&|*/%+-]\|\%(class\s*\)\@<!<<\|<=>\|<=\|\%(<\|\<class\s\+\u\w*\s*\)\@<!<[^<]\@=\|===\|==\|=\~\|>>\|>=\|=\@<!>\|\*\*\|\.\.\.\|\.\.\|::\)"
"syn match  rubyPseudoOperator  "\%(-=\|/=\|\*\*=\|\*=\|&&=\|&=\|&&\|||=\||=\|||\|%=\|+=\|!\~\|!=\)"
"syn region rubyBracketOperator matchgroup=rubyOperator start="\%(\w[?!]\=\|[]})]\)\@<=\[\s*" end="\s*]" contains=ALLBUT,@rubyNotTop
