" The following matches one or two characters, but no more (e.g. not +++)
syn match cOperator             "+\@<!+\{1,2}+\@!"
syn match cOperator             "-\@<!-\{1,2}-\@!"
syn match cOperator             "&\@<!&\{1,2}&\@!"
syn match cOperator             "&\@<!|\{1,2}&\@!"
syn match cOperator             "<\@<!<\{1,2}<\@!"
syn match cOperator             ">\@<!>\{1,2}>\@!"

" The following matches exactly one (e.g. not //)
syn match cOperator             "[*/]\@<!/[*/]\@!"
syn match cOperator             "[*/]\@<!\*[*/]\@!"
syn match cOperator             "!\@<!!!\@!"

" The following matches exactly two (e.g. not = or ===)
syn match cOperator             "=\@<!==[=]\@!"
syn match cOperator             "=\@<!<=[<]\@!"
syn match cOperator             "=\@<!>=[>]\@!"
syn match cOperator             "=\@<!!=[!]\@!"

syn match cArgumentSeparator    "," contained

hi def link cArgumentSeparator Operator
