syn clear

syn match sedComment    "#.*$"
syn match sedOperator   "\\\@<!/"
syn match sedST         "^[ \t]*[sy]/\@="
syn match sedFlag       "/\@<=g"
syn match sedConstant   "[$^*.()?+]"
syn match sedConstant   "\\[0-9]\+"
syn match sedConstant   "\\."
syn match sedConstant   "\\x[0-9]\{2}"

syn region sedCharacterGroup start="\[" end="[^:]\]"
syn region sedCharacterGroup start="\[:" end=":\]"

syn match sedCharacterGroupOperator   "\^" containedin=sedCharacterGroup contained

hi def link sedOperator                 Operator
hi def link sedConstant                 Constant
hi def link sedCharacterGroup           Special
hi def link sedCharacterGroupOperator   Operator
