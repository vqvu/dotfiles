if &t_Co == 8 || &t_Co == 16
  hi      dircolorsBlack      ctermfg=0
  hi      dircolorsRed        ctermfg=1
  hi      dircolorsGreen      ctermfg=2
  hi      dircolorsYellow     ctermfg=3
  hi      dircolorsBlue       ctermfg=4
  hi      dircolorsMagenta    ctermfg=5
  hi      dircolorsCyan       ctermfg=6
  hi      dircolorsWhite      ctermfg=7

  hi      dircolorsBGBlack    ctermbg=0
  hi      dircolorsBGRed      ctermbg=1
  hi      dircolorsBGGreen    ctermbg=2
  hi      dircolorsBGYellow   ctermbg=3
  hi      dircolorsBGBlue     ctermbg=4
  hi      dircolorsBGMagenta  ctermbg=5
  hi      dircolorsBGCyan     ctermbg=6
  hi      dircolorsBGWhite    ctermbg=7
endif
