" Makefrag => Makefile type
if exists("did_load_filetypes")
    finish
endif

augroup filetypedetect
    au! BufRead,BufNewFile Makefrag             setfiletype make
    au! BufRead,BufNewFile *.xfrag              setfiletype xhtml
    au! BufRead,BufNewFile *.erb                setfiletype eruby
    au! BufRead,BufNewFile *.frag,*.vert,*.fp,*.vp,*.glsl setfiletype glsl 
    au! BufRead,BufNewFile *.less               setfiletype less
augroup END

