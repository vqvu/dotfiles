" Make vim useful
set nocompatible

" Make backspace work
set backspace=indent,eol,start

" Source plugins
source ~/.vim/vundle.vim

" Enable file detection
filetype plugin indent on

" Default indentation
" set autoindent       " Use same indentation as previous line
"set tabstop=4        " Set tab character to 4 characters
set expandtab        " Turn tabs into whitespace
set shiftwidth=4     " Indent width for autoindent
set softtabstop=4    " Same as shiftwidth?

" Set color scheme
set t_Co=16
syntax enable

if $SHELL_ENV == 'default'
    set background=dark
    colorscheme solarized
elseif $SHELL_ENV == 'putty'
    colorscheme desert
endif

" Highlight all matching strings
set hls

set noundofile
set nobackup

" Turn of the use of the mouse.
set mouse=

" YCM config
let g:ycm_confirm_extra_conf = 0

if !exists("autocommands_loaded")
    let autocommands_loaded = 1

    " Set indent based on filetypes
    " Uncomment if not using ~/.vim/after/<filetype>.vim
    "autocmd FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2
    "autocmd FileType eruby setlocal shiftwidth=2 tabstop=2 softtabstop=2
    "autocmd FileType ruby setlocal shiftwidth=2 tabstop=2 softtabstop=2
    autocmd FileType java,c,cpp     setlocal textwidth=80
    autocmd FileType tex,markdown            setlocal textwidth=80 spell
    autocmd FileType xhtml,xml,html,xsd,json,yaml,jade,swig,html.handlebars,htmldjango,ruby   setlocal shiftwidth=2 softtabstop=2
    autocmd FileType gitconfig setlocal noexpandtab softtabstop=8 tabstop=8 shiftwidth=8
    autocmd FileType sh setlocal softtabstop=2 tabstop=2 shiftwidth=2

    augroup customftdetect
        au! BufRead,BufNewFile *.md setlocal filetype=markdown
        au! BufRead,BufNewFile *.swig setlocal filetype=htmldjango
        au! BufRead,BufNewFile *.god setlocal filetype=ruby
        au! BufRead,BufNewFile *.grb setlocal filetype=ruby
        au! BufRead,BufNewFile *vercomp*/*.pm  setlocal shiftwidth=2 softtabstop=2
        au! BufRead,BufNewFile *vercomp*/*.pl  setlocal shiftwidth=2 softtabstop=2
        au! BufRead,BufNewFile *vercomp*/*.py  setlocal shiftwidth=2 softtabstop=2
        au! BufRead,BufNewFile *vercomp*/*.cpp  setlocal shiftwidth=2 softtabstop=2
        au! BufRead,BufNewFile *vercomp*/*.hpp  setlocal shiftwidth=2 softtabstop=2
        au! BufRead,BufNewFile *vercomp*/*.h    setlocal shiftwidth=2 softtabstop=2
        au! BufRead,BufNewFile *vercomp*/*.cu   setlocal shiftwidth=2 softtabstop=2
        au! BufRead,BufNewFile */439/student-labs/*   setlocal noexpandtab shiftwidth=8 softtabstop=8
        au! BufRead,BufNewFile *.erb            call SetTypoShortcuts()
    augroup END

endif

" Set line numbering to take up 5 spaces
set number          " Turn on line numbers.
set numberwidth=5
set ignorecase      " Case insensitive searching.
set smartcase       " Unless I really mean case sensitive.
"set cursorline      " Highlight the current line.

" Set up folding configuration.
set foldmethod=syntax  " Fold based on syntax.
set foldlevelstart=99  " Don't fold by default.

" Map Ctrl+c/Ctrl+v/Ctrl+x appropriately
noremap <C-c> "+y<CR>
inoremap <C-c> <Esc><C-c>i

noremap <C-v> "+p<CR>
inoremap <C-v> <Esc><C-v>i

noremap <C-x> "+d<CR>
inoremap <C-x> <Esc><C-x>i

" Ctrl+s for save!!
noremap <C-s> :w<CR>
inoremap <C-s> <Esc>:w<CR>i

" Ctrl+z for undo
" noremap <C-z> u
" inoremap <C-z> <Esc>:u<CR>i

" Press i to enter insert mode and jj to exit
inoremap jj <esc>

" Home and End.
"noremap gl <END>
"inoremap gl <END>
"vnoremap gl <END>
noremap <C-l> <END>
inoremap <C-l> <END>
vnoremap <C-l> <END>

"noremap gh <Home>
"inoremap gh <Home>
"vnoremap gh <Home>
noremap <C-h> ^
inoremap <C-h> ^
vnoremap <C-h> ^

" Start nerd tree
"noremap <F2> :NERDTreeToggle<CR>

" Vim sudo trick. Use :w!!
cmap w!! w !sudo tee > /dev/null %

" vim-airline
set laststatus=2 " Always display status line.
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"

" Typography, only in erb files for now.
function! SetTypoShortcuts()
    set spell           " Enable spellchecking

    " It's html so set the tab width to 2.
    setlocal shiftwidth=2 softtabstop=2

    "— mdash
    "inoremap -- <%= @mdash %>
    "inoremap ... <%= @ldots %>
    inoremap uu ū
    inoremap cc ç
    inoremap <! <!--
    inoremap -> -->
    inoremap <C-o> ō

    inoremap <C-p> \opt-paragraph
    noremap <C-p> i\opt-paragraph<Esc>
    vnoremap <C-p> di<CR>\opt-paragraph<CR><Esc>

    " lErb for latex+erb
    syn match lErbMacro "\\[a-z0-9:\-]\+" contains=lErbMacroOperator nextgroup=lErbArgs skipwhite skipempty
    syn match lErbMacroOperator "::" contained

    syn region lErbArgs matchgroup=Delimiter start="{" skip="\\\\\|\\[{}]" end="}" contains=@Spell,htmlComment nextgroup=lErbArgs skipwhite skipempty contained
    " syn match  lErbError "[}\])]" contained

    syn match lErbKeyword "`"
    syn match lErbKeyword "'"
    syn match lErbKeyword "--"
    syn match lErbKeyword "---"
    syn match lErbKeyword "\.\.\."

    hi def link lErbArgs String
    hi def link lErbError Error
    hi def link lErbKeyword Keyword
    hi def link lErbMacro Identifier
    hi def link lErbMacroOperator Operator
endfunction

""""""""""
" Amazon "
""""""""""

" Function to set the screen title
function! SetTitle()
    let l:title = 'vi: ' . expand('%:t')

    if (l:title != 'vi: __Tag_List__')
        let l:truncTitle = strpart(l:title, 0, 15)
        silent exe '!echo -e -n "\033k' . escape(l:truncTitle, '!') . '\033\\"'
    endif
endfunction

" Run it every time we change buffers
autocmd BufEnter * call SetTitle()

