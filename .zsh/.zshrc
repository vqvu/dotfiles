# If not running interactively, don't do anything
[[ -z "$PS1" ]] && return

ENV_STATE_PATH="${HOME}/.env-state"

# Load standard functions.
source "${SHELL_FUNCTIONS_ROOT}/functions.sh"

# Set the PATH to point to point to GNU tools
is_mac && export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"

# Set up LD_LIBRARY_PATH
if is_mac; then
    export DYLD_LIBRARY_PATH="${HOME}/.local/lib:${DYLD_LIBRARY_PATH}"
else
    export LD_LIBRARY_PATH="${HOME}/.local/lib:${DYLD_LIBRARY_PATH}"
fi

# Source Prezto.
source "${DOTFILES_PATH}/external/prezto/init.zsh"

# Completion configs
# See https://zsh.sourceforge.io/Doc/Release/Completion-System.html#Standard-Styles
zstyle ':completion:*' show-completer true

#
# Functions
#
shell_env() {
    if [[ $# -ne 1 ]]; then
        echo "Usage: shell_env <env>"
        return 1
    fi

    export SHELL_ENV="$1"
    case "$1" in
    default)
        prompt agnoster

        # Set up customized colors for ls (if any)
        if [ -f ${HOME}/.dircolors ]; then
            eval $(dircolors ${HOME}/.dircolors)
        fi
        ;;
    putty)
        prompt giddie
        eval $(dircolors)
        ;;
    *)
        echo "$1 is not a valid environment."
        ;;
    esac
}

# Customize based on environment
shell_env "${SHELL_ENV:-default}"

# Set up default editor
export EDITOR=vim

# Have SWT use GTK3
#export SWT_GTK3=1

# Lots of (my) scripts check for this.
export MAVEN=maven

# Customize vim mode.
bindkey -M 'viins' '^r' history-incremental-search-backward  # Ctrl+r to search history
bindkey -M 'viins' 'jj' vi-cmd-mode  # jj to go to command mode
bindkey -M 'viins' '^j' down-line-or-history  # Ctrl+j <-> down-arrow
bindkey -M 'viins' '^k' up-line-or-history    # Ctrl+k <-> up-arrow

# Set up dev env bindings
[[ -f ~/scripts/setup-dev-env.sh ]] && source ~/scripts/setup-dev-env.sh

# Override certain prezto defaults
alias rm="nocorrect rm -I"

###     ###
# Aliases #
###     ###

# Add an "alert" alias for long running commands.  Use like so:
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Disable XON/XOFF flow control
stty -ixon

# enable color support of ls and also add handy aliases
# alias ls='ls --color=auto'
#alias dir='dir --color=auto'
#alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias rgrep='grep --color=auto -r'
alias fgrep='grep --color=auto -F'
alias egrep='grep --color=auto -E'

# some more ls aliases
alias la='ls -la'

# Display disk usage
alias duse='du -sk'

# GNU Screen aliases
alias sc=screen
alias scr="screen -dr"
alias scm="screen -X next"
alias scn="screen -X prev"
alias sck="screen -X kill"
alias scd="screen -X detach"

# Git alias
alias gst="git status"
alias gcm="git commit -v"
alias ga="git add"
alias gco="git checkout"
alias grm="git rm"

#
# Custom command overides
#
custom_override() {
    if [[ ( $# -lt 2 ) || ( $# -gt 3 ) ]]; then
        echo "Usage: custom_override <command> <override_fn> [completion_fn]"
    fi

    local cmd override completion
    cmd="$1"
    override="$2"
    [[ $# -ge 3 ]] && completion="$3" || completion=_gnu_generic

    alias ${cmd}="${override}"
    compdef "${completion}" "${override}"
}

# Override ag so we can fix its colors for solarized.
# Only do if ag is installed.
_override_ag() {
    if [[ "${SOLARIZED_ENV}" = true ]]; then
        \ag --color-line-number '0;33' --color-path '0;32' "$@"
    else
        \ag "$@"
    fi
}

custom_override ag _override_ag

