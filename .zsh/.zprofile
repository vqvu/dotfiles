source "${SHELL_FUNCTIONS_ROOT}/env.sh"

# Ensure path arrays do not contain duplicates.
typeset -gU cdpath fpath mailpath path

if is_wsl; then
  # Simplify the path when using WSL.
  # This makes auto completion much faster since we don't need to look at
  # Windows files.
  path=(
    /usr/local/sbin
    /usr/local/bin
    /usr/sbin
    /usr/bin
    /sbin
    /bin
    /usr/games
    /usr/local/games
    /usr/lib/wsl/lib
  )
fi

path=(
  "${HOME}/.local/bin"
  $path
)

if [[ -s "${ZDOTDIR}/local.zprofile" ]]; then
    source "${ZDOTDIR}/local.zprofile"
fi

