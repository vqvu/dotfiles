init_dir() {
    local dir_name dir
    dir_name="$1"
    dir="$2"

    if [[ ! -e "${dir}" ]]; then
        info_msg "Creating ${dir_name} directory: ${dir}."
        mkdir -p "${dir}"
    elif [[ ! -d "${dir}" ]]; then
        error_msg "The ${dir_name} path (${dir}) is not a directory."
    else
        success_msg "The ${dir_name} directory already exists."
    fi
}

init_basic_setup_deps() {
    local current previous tmp
    previous=
    current="${DOTFILES_PATH}"

    if [[ ! -e "${current}" ]]; then
        # This is pre-bootstrap, so we can't use link
        info_msg "Creating dotfiles entrypoint symlink: ${DOTFILES_PATH}."
        ln -s "${CONFIG_PATH}" "${DOTFILES_PATH}"
        return 0
    fi

    while [[ "z${previous}" != "z${current}" && -L "${current}" ]]; do
        previous="${current}"
        current="$(readlink "${current}")"
    done

    if [[ "z${current}" != "z${CONFIG_PATH}" ]]; then
        error_msg "${DOTFILES_PATH} is not a symlink that points to ${CONFIG_PATH}."
    else
        success_msg "dotfiles entrypoint already installed."
    fi

    if [[ ! -d "${DOTFILES_EXTERNAL_PATH}" ]]; then
        error_msg "The external deps path (${DOTFILES_EXTERNAL_PATH}) doesn't exist or is not a directory."
    fi

    init_dir "installation metadata" "${INSTALL_META_PATH}"
    init_dir "installation cache" "${CACHE_PATH}"
    init_dir "local install" "${LOCAL_INSTALL_PATH}"

    for subdir in bin lib share; do
        init_dir "local install ${subdir}" "${LOCAL_INSTALL_PATH}/${subdir}"
    done

    if [[ -e "${SCRATCH_PATH}" ]]; then
        error_msg "The installation scratch path (${SCRATCH_PATH}) exists. Delete it and allow this script to create it."
    else
        info_msg "Creating installation scratch directory: ${SCRATCH_PATH}."
        mkdir -p "${SCRATCH_PATH}"
        _DELETE_SCRATCH=true
    fi
}

header_msg "Initializing installation environment."
init_basic_setup_deps

if is_linux; then
    # Cache sudo.
    info_msg "Caching sudo password..."
    sudo echo > /dev/null

    apt_install curl
fi

exec_all_modules pre-network-setup

